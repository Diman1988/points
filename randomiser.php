<?php
require('point.php');
require('item.php');
require('money.php');

class Randomiser {
    public function getType( $id ) {
        $random_number = $id;
        if($random_number == 0) {
            return Point::name;
        }
        if($random_number == 1) {
            return Item::name;
        }
        if($random_number == 2) {
            return Money::name;
        }
    }
    
    public function getGift() {
        $random_number = rand(0,2);

        return $random_number;
    }
}