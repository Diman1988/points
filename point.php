<?php
require('interface.php');

class Point implements GiftInterface {
    const name = 'points';

    public function sendGift( $quantity )
    {
        $file_name = 'points.txt';
        if(is_file($file_name)) {
            $json = file_get_contents( $file_name, 0, null, null );
            $json = $json + $quantity;

            if(file_put_contents( $file_name, print_r ( $json, true ) ) != false) {
                return true;
                exit;
            }
        }
        return false;
    }

    public function getGift() {
        $file_name = 'points.txt';
        if(is_file($file_name)) {
            $json = file_get_contents( $file_name, 0, null, null );
        }
        return $json;
    }
}