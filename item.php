<?php

class Item implements GiftInterface {
    const name = 'item';

    public function sendGift()
    {
        //Delete element from items.json
    }

    private function getList() { //get maximum avaliable items list
        $file_name = 'items.json';
        if(is_file($file_name)) {
            $json = file_get_contents( $file_name, 0, null, null );
            $json = json_decode( $json );
            return $json;
        } else {
            return false;
        }

    }

    public function getGift() {
        $list = $this->getList();
        $count = count($list);
        $winitem = rand(0, $count);

        return $list[$winitem];
    }
}