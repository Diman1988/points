<?php

class Money implements GiftInterface {
    const name = 'money';

    public function sendGift( $banknumber, $quantity) {
        $bank_url = 'https://api.bank.bnk/add/money?banknumber=' . urlencode($banknumber) . '&quantity=' . urlencode($quantity);
        $bank_json = file_get_contents($bank_url);

        return $bank_json;
    }

    public function convertTo( $quantity ) {
        $k = 0.5;
        $points = $quantity * $k;
        Point::sendGift($points);

        return true;
    }

    private function getList() { //Get maximum avaliable money
        $file_name = 'money.txt';
        if(is_file($file_name)) {
            $json = file_get_contents( $file_name, 0, null, null );
            return $json;
        } else {
            return false;
        }

    }

    public function getGift() {
        $max_limit = self::getList();
        $win = rand(0, $max_limit);

        return $win;
    }
}